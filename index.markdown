---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# FabLab Kamp-Linfort 2022

## Students
- [Aaron Hinkle](https://fabacademy.org/2022/labs/kamplintfort/students/aaron-hinkle/) 
- [Jan Bewersdorff](https://fabacademy.org/2022/labs/kamplintfort/students/jan-bewersdorff/)
- [Leen Nijim](https://fabacademy.org/2022/labs/kamplintfort/students/leen-nijim/)
- [Roland Grichnik](https://fabacademy.org/2022/labs/kamplintfort/students/roland-grichnik/)
- [Lisa Schilling](https://fabacademy.org/2022/labs/kamplintfort/students/lisa-schilling/)


## Instructors
- Ahmed Belal
- Daniele Ingrassia
- Marcello Tania
